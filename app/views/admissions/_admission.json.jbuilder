json.extract! admission, :id, :moment, :created_at, :updated_at
json.url admission_url(admission, format: :json)

Rails.application.routes.draw do
  resources :admissions
  resources :patients
  resources :diagnoses
  resources :diagnostic_procedures
  resources :order_frequencies
  resources :treatments
  resources :medication_orders
  resources :allergies
  resources :observations
  resources :symptoms
  resources :facilities
  resources :chronic_conditions
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root "patients#list"
end

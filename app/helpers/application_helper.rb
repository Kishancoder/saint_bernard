module ApplicationHelper

  def medication_orders(patient)
    patient.medication_orders.each_with_object([]) do |e, r|
      r << "#{e.name} #{e.dosage} #{e.route} #{e.order_frequency.value}"
    end.join(', ')
  end

  def bootstrap_class_for flash_type
    case flash_type.to_sym
      when :success
        "alert-success"
      when :error
        "alert-danger"
      when :alert
        "alert-info"
      when :notice
        "alert-success"
      else
        flash_type.to_s
    end
  end
end

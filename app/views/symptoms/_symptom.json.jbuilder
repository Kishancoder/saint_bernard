json.extract! symptom, :id, :admission_id, :description, :created_at, :updated_at
json.url symptom_url(symptom, format: :json)

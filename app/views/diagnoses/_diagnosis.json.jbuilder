json.extract! diagnosis, :id, :diagnosisable_id, :coding_system, :code, :description, :type, :created_at, :updated_at
json.url diagnosis_url(diagnosis, format: :json)

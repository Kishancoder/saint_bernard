class PatientMedicationOrder < ApplicationRecord
  belongs_to :patient, optional: true
  belongs_to :medication_order, optional: true
end

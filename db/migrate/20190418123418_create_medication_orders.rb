class CreateMedicationOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :medication_orders do |t|
      t.belongs_to :patient
      t.string :name
      t.integer :unit
      t.decimal :dosage
      t.integer :route
      t.text :necessity
      t.belongs_to :frequency

      t.timestamps
    end
  end
end

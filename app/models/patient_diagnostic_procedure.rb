class PatientDiagnosticProcedure < ApplicationRecord
  belongs_to :patient, optional: true
  belongs_to :diagnostic_procedure, optional: true
end

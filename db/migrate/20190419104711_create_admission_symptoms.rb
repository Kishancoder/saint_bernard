class CreateAdmissionSymptoms < ActiveRecord::Migration[5.1]
  def change
    create_table :admission_symptoms do |t|
      t.belongs_to :admission, index: true
      t.belongs_to :symptom, index: true

      t.timestamps
    end
  end
end

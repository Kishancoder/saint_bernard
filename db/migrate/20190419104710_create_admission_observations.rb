class CreateAdmissionObservations < ActiveRecord::Migration[5.1]
  def change
    create_table :admission_observations do |t|

      t.belongs_to :admission, index: true
      t.belongs_to :observation, index: true

      t.timestamps
    end
  end
end

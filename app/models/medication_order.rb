class MedicationOrder < ApplicationRecord
  belongs_to :order_frequency, foreign_key: :frequency_id

  enum unit: {MG: 0}
  enum route: {PO: 0, IM: 1, SC: 2}
end

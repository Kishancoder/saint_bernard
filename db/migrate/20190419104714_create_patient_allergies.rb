class CreatePatientAllergies < ActiveRecord::Migration[5.1]
  def change
    create_table :patient_allergies do |t|
      t.belongs_to :patient, index: true
      t.belongs_to :allergy, index: true
      t.timestamps
    end
  end
end

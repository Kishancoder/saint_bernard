class CreatePatientMedicationOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :patient_medication_orders do |t|
      t.belongs_to :patient, index: true
      t.belongs_to :medication_order, index: true

      t.timestamps
    end
  end
end

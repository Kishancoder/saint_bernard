# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Facility.create!(name: 'Star Wars')

a1 = Admission.create!(moment: DateTime.now)
a2 = Admission.create!(moment: DateTime.now - 5.day)

p1 = Patient.create!(first_name: "Same",
                middle_name: "Jack",
                last_name: "Zone",
                dob: Time.now - 20.year,
                gender: "Male",
                mr: '123456',
                admission: a1
)

p2 = Patient.create!(first_name: "John",
                middle_name: "Jack",
                last_name: "Jack",
                dob: Time.now - 25.year,
                gender: "Female",
                mr: '754',
                admission: a2
)




Allergy.create!(patient: p1, description: "Allergy 1")
Allergy.create!(patient: p2, description: "Allergy 2")
Allergy.create!(patient: p1, description: "Allergy 3")

Diagnosis.create!(diagnosisable: a1, code: '1253', description: "Diagnosis 1")
Diagnosis.create!(diagnosisable: a2, code: '4456', description: "Diagnosis 2")
Diagnosis.create!(diagnosisable: p1, code: '1273', description: "Diagnosis 3")
Diagnosis.create!(diagnosisable: p1, code: '4596', description: "Diagnosis 4")
ChronicCondition.create!(diagnosisable: p2, code: '789', description: "ChronicCondition 4")

DiagnosticProcedure.create!(patient: p1, moment: DateTime.now, description: "DiagnosticProcedure 1")
DiagnosticProcedure.create!(patient: p2, moment: DateTime.now - 5.day, description: "DiagnosticProcedure 2")

Observation.create!(admission: a1, description: "Observation 1", moment: DateTime.now)
Observation.create!(admission: a2, description: "Observation 2", moment: DateTime.now - 5.day)

Symptom.create!(admission: a1, description: "Symptom 1")
Symptom.create!(admission: a2, description: "Symptom 2")

Treatment.create!(patient: p1, description: "Treatment 1", necessity: 'seriously')
Treatment.create!(patient: p2, description: "Treatment 2", necessity: 'normal')

of1 = OrderFrequency.create(unit: "Hour", value: 2)
of2 = OrderFrequency.create(unit: "Hour", value: 4)

MedicationOrder.create!(order_frequency: of1, patient: p1, name: 'peracita', dosage: '1 time')
MedicationOrder.create!(order_frequency: of2, patient: p2, name: 'nimeson', dosage: '2 time')



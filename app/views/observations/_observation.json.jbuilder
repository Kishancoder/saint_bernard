json.extract! observation, :id, :admission_id, :description, :moment, :created_at, :updated_at
json.url observation_url(observation, format: :json)

json.extract! order_frequency, :id, :value, :unit, :created_at, :updated_at
json.url order_frequency_url(order_frequency, format: :json)

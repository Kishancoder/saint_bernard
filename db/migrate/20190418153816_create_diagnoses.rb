class CreateDiagnoses < ActiveRecord::Migration[5.1]
  def change
    create_table :diagnoses do |t|
      t.belongs_to :diagnosisable, polymorphic: true
      t.string :coding_system
      t.string :code
      t.text :description
      t.string :type

      t.timestamps
    end
  end
end

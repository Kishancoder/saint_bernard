class CreateAdmissionDiagnoses < ActiveRecord::Migration[5.1]
  def change
    create_table :admission_diagnoses do |t|
      t.belongs_to :admission, index: true
      t.belongs_to :diagnosis, index: true

      t.timestamps
    end
  end
end

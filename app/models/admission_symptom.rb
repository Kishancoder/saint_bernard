class AdmissionSymptom < ApplicationRecord
  belongs_to :admission, optional: true
  belongs_to :symptom, optional: true
end

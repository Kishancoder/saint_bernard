class CreatePatientDiagnoses < ActiveRecord::Migration[5.1]
  def change
    create_table :patient_diagnoses do |t|
      t.belongs_to :patient, index: true
      t.belongs_to :diagnosis, index: true

      t.timestamps
    end
  end
end

json.extract! medication_order, :id, :patient_id, :name, :unit, :dosage, :route, :necessity, :frequency_id, :created_at, :updated_at
json.url medication_order_url(medication_order, format: :json)

class AdmissionObservation < ApplicationRecord
  belongs_to :admission, optional: true
  belongs_to :observation, optional: true
end

class CreatePatientDiagnosticProcedures < ActiveRecord::Migration[5.1]
  def change
    create_table :patient_diagnostic_procedures do |t|
      t.belongs_to :patient, index: true
      t.belongs_to :diagnostic_procedure, index: true

      t.timestamps
    end
  end
end

class CreatePatientChronicConditions < ActiveRecord::Migration[5.1]
  def change
    create_table :patient_chronic_conditions do |t|

      t.belongs_to :patient, index: true
      t.belongs_to :chronic_condition, index: true

      t.timestamps
    end
  end
end

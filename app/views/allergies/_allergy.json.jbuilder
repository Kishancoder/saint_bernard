json.extract! allergy, :id, :patient_id, :description, :created_at, :updated_at
json.url allergy_url(allergy, format: :json)

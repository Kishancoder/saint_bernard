class PatientDiagnosis < ApplicationRecord
  belongs_to :patient, optional: true
  belongs_to :diagnosis, optional: true
end

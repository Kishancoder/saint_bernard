class AdmissionDiagnosis < ApplicationRecord
  belongs_to :admission, optional: true
  belongs_to :diagnosis, optional: true
end
